//
//  ErrorManager.swift
//  WeatherApplication
//
//  Created by Oleksiy Irzhavsky on 21.01.2021.
//

import Foundation

public let SWINetworkingErrorDomain = "ru.swiftbook.WeatherApp.NetworkingError"
public let MissingHTTPResponseError = 100
public let UnexpectedResponseError = 200
